class AddScrappedAtToStories < ActiveRecord::Migration[5.2]
  def change
    add_column :stories, :scrapped_at, :datetime
  end
end
