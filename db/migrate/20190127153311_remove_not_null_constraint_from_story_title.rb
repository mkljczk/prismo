class RemoveNotNullConstraintFromStoryTitle < ActiveRecord::Migration[5.2]
  def change
    change_column :stories, :title, :string, null: true
  end
end
