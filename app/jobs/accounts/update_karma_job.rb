module Accounts
  class UpdateKarmaJob < ApplicationJob
    queue_as :default

    def perform(account_id, resource_type, add_or_remove = 'add')
      account = Account.find(account_id)
      method = add_or_remove == 'add' ? :increment! : :decrement!

      case resource_type
      when 'Comment' then account.send(method, :comments_karma)
      when 'Story' then account.send(method, :posts_karma)
      else logger.warn "Unknown resource type #{resource_type}"
      end

      true
    end
  end
end
