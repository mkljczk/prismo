class ManifestSerializer < Granola::Serializer
  include RoutingHelper

  def initialize
    super(nil)
  end

  def data
    {
      name: Setting.site_title,
      short_name: Setting.site_title,
      description: Setting.site_description,
      background_color: '#f9f9f9',
      theme_color: '#16a1b5',
      start_url: '.',
      scope: '/',
      display: 'standalone',
      icons: icons,
    }
  end

  def icons
    [
      {
        src: '/favicon.png',
        sizes: '16x16',
        type: 'image/png',
      }
    ]
  end
end
