class StoryPolicy < ApplicationPolicy
  def index?
    true
  end

  def create?
    user.present? && !user.account.silenced? && !user.account.suspended?
  end

  def edit?
    user.present? && (user.is_admin? || user == account.user)
  end

  def update?
    edit?
  end

  def scrap?
    user.present? && (user.is_admin? || user == record.account.user)
  end

  def comment?
    user.present? && !user.account.silenced? && !user.account.suspended?
  end

  def toggle_like?
    user.present? && !user.account.silenced? && !user.account.suspended?
  end
end
