class Api::V1::ToolsController < Api::V1Controller
  def scrap_url
    cache_key = Digest::MD5.hexdigest params[:url]

    scrapper = Rails.cache.fetch("link_thumbnailer/#{cache_key}", expires_in: 1.hour) do
      LinkThumbnailer.generate(params[:url])
    end

    render json: scrapper

  rescue LinkThumbnailer::BadUriFormat => e
    render json: { errors: ['URL seems to be invalid'] }, status: :bad_request
  rescue LinkThumbnailer::HTTPError => e
    render json: { errors: ['URL seems to be invalid'] }, status: :bad_request
  end
end
