class Api::Ujs::StoriesController < Api::Ujs::BaseController
  def toggle_like
    user_needed
    story = find_story
    authorize story

    outcome = Stories::ToggleLike.run(story: story, account: current_user.account)
    set_account_liked_story_ids

    render 'stories/_story', layout: false, locals: { story: outcome.result.likeable }
  end

  def scrap
    story = find_story
    authorize story, :scrap?

    Stories::Rescrap.run(story: story, user: current_user)

    head :no_content
  end

  private

  def find_story
    Story.find(params[:id])
  end
end
