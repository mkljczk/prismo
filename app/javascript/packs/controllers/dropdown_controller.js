import BaseController from "./base_controller"
import Popper from 'popper.js'

export default class extends BaseController {
  static targets = ['toggle', 'dropdown']

  connect() {
    this._popper = new Popper(this.toggleTarget, this.dropdownTarget, {
      placement: this.placement,
      eventsEnabled: false
    })
  }

  disconnect() {
    this.opened = false
    this._popper.destroy()
  }

  toggle (e) {
    e.preventDefault()
    this.opened = !this.dropdownTarget.classList.contains('menu-n--opened')
  }

  hide (e) {
    if (this.element.contains(e.target) == false) {
      this.opened = false
    }
  }

  get placement () {
    return this.data.get('placement') || 'bottom-start'
  }

  set opened (value) {
    if (value) {
      this._popper.enableEventListeners()
      this._popper.update()
      this.dropdownTarget.classList.add('menu-n--opened')
    } else {
      this.dropdownTarget.classList.remove('menu-n--opened')
      this._popper.disableEventListeners()
    }
  }
}
