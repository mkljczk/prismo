class Comments::ToggleLike < ActiveInteraction::Base
  object :comment
  object :account

  def execute
    if account.liked?(comment)
      compose(Comments::Unlike, inputs)
    else
      compose(Comments::Like, inputs)
    end
  end
end
