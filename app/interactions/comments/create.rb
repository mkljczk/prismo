class Comments::Create < ActiveInteraction::Base
  string :uri, default: nil
  string :url, default: nil
  integer :parent_id, default: nil
  integer :story_id
  string :body, default: nil
  string :created_at, default: nil
  object :account
  boolean :local, default: nil
  boolean :auto_like, default: true

  validates :body, presence: true

  def execute
    parent = parent_id.present? ? Comment.find(parent_id) : nil
    story  = Story.find(story_id)

    comment = Comment.new
    comment.uri = uri
    comment.url = url
    comment.parent_id = parent.id if parent.present?
    comment.story_id = story.id
    comment.body = body
    comment.created_at = created_at
    comment.account = account
    comment.local = local
    comment.domain = domain_from_uri

    if comment.save
      after_comment_create(comment)
    else
      errors.merge!(comment.errors)
    end

    comment
  end

  private

  def domain_from_uri
    return nil if uri.blank?
    URI.parse(uri).host
  end

  def after_comment_create(comment)
    comment.cache_depth
    comment.cache_body
    account.touch(:last_active_at)

    Comments::Like.run!(comment: comment, account: account) if auto_like
    Comments::BroadcastCreation.run! comment: comment
    Notifications::SendForComment.run! comment: comment
    ActivityPub::DistributionJob.call(comment) if local
  end
end
