class Comments::Delete < ActiveInteraction::Base
  object :comment

  def execute
    comment.update(
      account: nil,
      body: nil,
      body_html: nil,
      uri: nil,
      url: nil,
      domain: nil,
      removed: true
    )

    comment
  end
end
