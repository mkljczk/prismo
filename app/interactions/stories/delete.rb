class Stories::Delete < ActiveInteraction::Base
  object :story

  def execute
    story.update!(
      account: nil,
      title: nil,
      description: nil,
      uri: nil,
      url: nil,
      url_meta: nil,
      url_domain: nil,
      domain: nil,
      removed: true
    )

    story
  end
end
