require 'rails_helper'

describe SuspendAccountService do
  describe '#call' do
    before do
      stub_request(:post, 'https://alice.com/inbox').to_return(status: 201)
      stub_request(:post, 'https://bob.com/inbox').to_return(status: 201)
    end

    subject do
      described_class.new.call(account)
    end

    let!(:account) { create(:account) }
    let!(:story) { create(:story, account: account) }
    let!(:comment) { create(:comment, account: account) }
    let!(:like) { create(:like, account: account) }
    let!(:notification) { create(:notification, recipient: account) }
    let!(:follow_request) { create(:follow_request, account: account) }
    let!(:active_follow) { create(:follow, account: account) }
    let!(:passive_follow) { create(:follow, target_account: account) }

    let!(:remote_alice) { create(:account, inbox_url: 'https://alice.com/inbox') }
    let!(:remote_bob) { create(:account, inbox_url: 'https://bob.com/inbox') }

    it 'soft deletes associated comments' do
      expect { subject }.to change { story.reload.removed? }.to(true)
    end

    it 'soft deletes associated stories' do
      expect { subject }.to change { comment.reload.removed? }.to(true)
    end

    it 'soft deletes likes' do
      expect { subject }.to change(account.likes, :count).by(-1)
    end

    it 'soft deletes notifications' do
      expect { subject }.to change(account.notifications_as_recipient, :count).by(-1)
    end

    it 'soft deletes follow requests' do
      expect { subject }.to change(account.follow_requests, :count).by(-1)
    end

    it 'soft deletes active follows' do
      expect { subject }.to change(account.active_follows, :count).by(-1)
    end

    it 'soft deletes passive follows' do
      expect { subject }.to change(account.passive_follows, :count).by(-1)
    end

    # @todo Move that to have_enqueued_job matcher
    it 'sends a delete actor activity to all known inboxes' do
      original_queue_adapter = ActiveJob::Base.queue_adapter
      ActiveJob::Base.queue_adapter = :inline

      subject
      expect(a_request(:post, 'https://alice.com/inbox')).to have_been_made
      expect(a_request(:post, 'https://bob.com/inbox')).to have_been_made

      ActiveJob::Base.queue_adapter = original_queue_adapter
    end
  end
end
