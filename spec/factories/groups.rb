FactoryBot.define do
  factory :group do
    sequence(:name) { |i| "username#{i}" }

    trait :super do
      supergroup true
    end
  end
end
