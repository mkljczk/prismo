FactoryBot.define do
  factory :user do
    sequence(:email) { |i| "email#{i}@example.com" }
    password 'TestPass'
    confirmed_at Time.zone.now

    trait :admin do
      is_admin true
    end

    trait :unconfirmed do
      confirmed_at nil
    end

    trait :with_account do
      account
    end
  end
end
